# Lil trivialities that at times help write few less letters or so
import numpy as np
from flib.flib import fs as fs
from flib.flib import fsfw as fsfw
from flib.flib import fd as fd
from flib.flib import fdsl as fdsl
from flib.flib import fdc as fdc
from flib.flib import ferr as ferr
import flib.fassert as fassert

def rngincl(start,stopIncl,step=None): # vector INCL last value, i.e.  [] instead of [)
    return np.arange(start,stopIncl+(step if step else 1),step)

import time
tictoctime = 0.0
tictocdisp = False
def tic(tictocdisp_: bool=None):
    global tictoctime
    tictoctime = time.time()
    if not (tictocdisp_ is None):
        global tictocdisp
        tictocdisp = tictocdisp_
def toc(s="",dispoverride=None):
    global tictoctime
    t = time.time()-tictoctime
    disp = tictocdisp if dispoverride is None else dispoverride
    if tictocdisp:
        fd(("" if s is "" else fs(s,": ")),round(t,4),"s")
    return t
def toti(s=""):
    t = toc(s)
    tic()
    return t

def simplesec(id:str=''):
    ''' Great auto-timer: Just add it in a loop anywhere you want to get the time-per-iteration, w/o further ado
    :param id: unique name argument, allowing to use nested/separate simplesec in different places
    :return:
    '''
    now = time.time()
    try:
        dsec = now - simplesec.store.get('id',np.nan)
        simplesec.store['id'] = now
        return dsec
    except: # Very first simplesec use; .store does not yet exist
        simplesec.store={id: now}
        return np.nan

def simpleTic(id:str=''):
    '''
    :param id: unique name argument, allowing to use nested/separate simplesec in different places
    :return:
    '''
    now = time.time()
    simpleToc.store[id] = now

def simpleToc(id:str=''):
    '''
    :param id: unique name argument, allowing to use nested/separate simplesec in different places
    :return:
    '''
    fassert.isin(id,simpleToc.store.keys(),'Please first run simpleTic({})'.format(id))
    now = time.time()
    return now-simpleToc.store[id]
simpleToc.store={}

def removeDot0(s): # Remove, in result, trailing ".0" if present at end of string s
    while (s[-1] in ["0","."]) and "." in s: s=s[:-1]
    return s

def smartround(val,relevantmagnitudes,strRmvDot0=False):
    """
    :param val: number
    :param relevantmagnitudes: int number
    :param strRmvDot0: whether convert into string and remove any trailing '.0..'
    :return: val rounded such as to preserve at least relevantmagnitudes relevant digits; if strRmvDot0 then string with possible '.0..' removed
        E.g. smartround(333.33,4)=333.3, smartround(333.3,2)=smartround(333.3,3)=333
    """
    if val>0:
        v = round(val, max(0, relevantmagnitudes - int(round(np.log10(val)))))
    elif val<0:
        v = -round(-val, max(0, relevantmagnitudes - int(round(np.log10(-val)))))
    elif val is 0:
        v = 0
    else:
        ferr("Unidentified value number type: \n",val,"\n",type(val))
    return removeDot0(str(v)) if strRmvDot0 else v

def smartroundRemoveDot0(val,relevantmagnitudes):
    s = removeDot0(str(smartround(val,relevantmagnitudes)))

def cycleR(x:list, n=1, inplace=False):
    '''Cycle (rotate) elements of list Rightwards'''
    fassert.f(isinstance(x,list),'x must be list')
    fassert.rng(n,0,len(x),'shift number n')
    if inplace:
        if n!=0:
            a = x[-n:]
            del x[-n:]
            x[0:0]=a # Useulf syntax described e.g. by https://codereview.stackexchange.com/a/83342/190545 and https://codereview.stackexchange.com/a/83339/190545
    else:
        if n==0:
            return x
        else:
            return x[-n:]+x[:-n]
def cycleL(x:list, n=1, inplace=False):
    '''Cycle (rotate) elements of list Leftwards'''
    fassert.f(isinstance(x,list),'x must be list, but is {} instead'.format(type(x)))
    fassert.rng(n,0,len(x),'shift number n')
    if inplace:
        if n!=0:
            a = x[:n]
            del x[:n]
            x.extend(a)
    else:
        if n==0:
            return x
        else:
            return x[n:]+x[:n]

def popappend(x:list, val):
    '''
    Pops first (left) element from list x and appends value val at end (right) of list, in-place. Popped value is returned
    Thus, shifts all elements by 1 step to the left, returning the lost first element and adding the element val at the end
    :param x:
    :param val:
    :return:
    '''
    pop = x.pop(0)
    x.append(val)
    return pop