## Testing flib components

# Mind, currently cannot be run fully automatically, as there's an inconvenient problem with
# making the flib package readily available/runnable for import in other places, and self-running stuff here within
# the package itself:
# E.g. 'import flib.flib as flib' vs. only 'import flib'; mind cannot change these things in e.g. flib.py, as else
# it won't readily run anymore in other places; and of course I prefer to keep the library intact for other uses, even
# at the cost of having to run the tests here with a bit manual copy pasting into console etc.

import datetime
import os
import errno
import numpy as np # just to be able to plot nicely, easily
# import fassert
# import flib
import fassert as fassert
import flib.flib as flib

# Test flib.MA
testdat = np.random.rand(100)
rad = 10
testMA = flib.MA(testdat,radius=rad,mode="NAN")
for i in range(rad,100-rad):
    fassert.eqtol(testMA,np.mean(testdat[i-rad:i+rad+1]))

# Test utctotimezone
format = '%Y-%m-%d %H:%M:%S'
raws = [
    datetime.datetime.strptime('2011-02-21 02:37:21', format),
    datetime.datetime.strptime('2011-07-21 02:37:21', format)
    ]
zone = 'Europe/Berlin'
hour_diffs = [
    1,
    2
]
for raw,hour_diff in zip(raws,hour_diffs):
    loc = utctotimezone(raw,zone)
    dateTimeDifference = loc - raw
    # Divide difference in seconds by number of seconds in hour (3600)
    dateTimeDifferenceInHours = dateTimeDifference.total_seconds() / 3600
    eq(dateTimeDifferenceInHours,hour_diff,"utctotimezone problem: Time difference not as expected")