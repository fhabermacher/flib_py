import psutil
import string
import random
import datetime
import pytz
from dateutil import tz
import os
import errno
import numpy as np # just to be able to plot nicely, easily
import flib.fassert as fassert
import sys
import matplotlib.pyplot as plt
import math
from os.path import isfile
import csv
import pathlib
import traceback
import collections
import glob
import time
## FString
class fdcolor: # Inspired by: https://stackoverflow.com/a/287944/3673329, http://ozzmaker.com/add-colour-to-text-in-python/, https://en.wikipedia.org/wiki/ANSI_escape_code
    Black=30 # Overall color ANSI str for Black will be '\033[30m'
    Red=31
    Green=32
    Yellow=33
    Blue=34
    Magenta=35
    Cyan=36
    White=37
    BrightBlack=90
    BrightRed=91
    BrightGreen=92
    BrightYellow=93
    BrightBlue=94
    BrightMagenta=95
    BrightCyan=96
    BrightWhite=97
    Bold=1
    Reset=0
    @staticmethod
    def fromstr(colstr):
        if colstr is "Black": return fdcolor.Black
        elif colstr is "Red": return fdcolor.Red
        elif colstr is "Green": return fdcolor.Green
        elif colstr is "Yellow": return fdcolor.Yellow
        elif colstr is "Blue": return fdcolor.Blue
        elif colstr is "Magenta": return fdcolor.Magenta
        elif colstr is "Cyan": return fdcolor.Cyan
        elif colstr is "White": return fdcolor.White
        elif colstr is "BrightBlack": return fdcolor.BrightBlack
        elif colstr is "BrightRed": return fdcolor.BrightRed
        elif colstr is "BrightGreen": return fdcolor.BrightGreen
        elif colstr is "BrightYellow": return fdcolor.BrightYellow
        elif colstr is "BrightBlue": return fdcolor.BrightBlue
        elif colstr is "BrightMagenta": return fdcolor.BrightMagenta
        elif colstr is "BrightCyan": return fdcolor.BrightCyan
        elif colstr is "BrightWhite": return fdcolor.BrightWhite
        elif colstr is "Bold": return fdcolor.Bold
        elif colstr is "Reset": return fdcolor.Reset
        else:
            fd("Unrecognized color string")
            exit()
    @staticmethod
    def full(color):
        if type(color) is str:
            color = fdcolor.fromstr(color)
        return fs('\033[',color,'m')

def flatlist(nestedlist,inplace: bool=False):
    fassert.f(isinstance(inplace,bool),'To avoid user confusion with arguments, we require inplace to strictly be bool')
    """Creates flat list from potentially nested lists. Sets and tuples are allowed iif not inplace"""
    flat = []
    for elem in nestedlist:
        if ((not inplace) and isinstance(elem,(list,set,tuple))) or isinstance(elem,list): # Iif not inplace, elem may also be set instead of list
            flat = flat + flatlist(elem)
        else:
            flat.append(elem)
    if inplace:
        # Inplace operation; .clear & .extend(flat) instead of =flat, to allow inplace change:
        # With nestedlist = flat, we would create a new var 'nestedlist' and loose the link with the original list passed
        # from outside into the function as a reference!
        nestedlist.clear()
        nestedlist.extend(flat)
        return None
    else:
        return flat

def fs(*arg):
    return ''.join([str(e) for e in arg])

def fsfw(width, *arg, just="left"):
    """
    Fixed-width string, padding with empty spaces. New version: Does not truncate anymore as less risky, and trivial enough to truncate from outside
    :param width:
    :param arg:
    :param just:
    :return:
    """
    s=fs(*arg)
    n = len(s)
    if just=="left": s=s.ljust(width)
    elif just=="right": s=s.rjust(width)
    elif just=="center": s=s.center(width)
    else: ferr("Unrecognized just param ",just)
    return s

_fdpar = {'file':None,'stdout':True,
          'alwaysflush':False, # Whether if file, flush (i.e. write to drive) each time
          'flushsec':[None, 10][1], # Whether if file, None or else time interval in seconds between flushes
          'lastsec': datetime.datetime.now().timestamp(), # If file, when last flushed
          }
_fdstatekey = set(_fdpar.keys()) # Parameters that can be 'set' and stored in _fdpar
_fdauxkey = ('clear','mkdir')
_fdstateandauxkey = flatlist([_fdstatekey,_fdauxkey])
_fdusagekey = ('flush','nostdout','nofile','clear','sameline','sl') # Parameters that can only be used in individual fd() (etc.) call, but not stored
_fdallkey = flatlist([_fdstateandauxkey,_fdusagekey])
def fdset(**kwargs):
    for k,v in kwargs.items():
        if k=='file':
            if _fdpar['file']:
                _fdpar['file'].close()
            if kwargs.get('clear',False) and isfileonly(v):
                os.remove(v)
            if kwargs.get('mkdir',False):
                mkdirIfNotExist(v)
            _fdpar['file'] = open(v,'a')
        elif k=='stdout':
            _fdpar['stdout'] = v
        elif k=='alwaysflush':
            _fdpar['alwaysflush'] = v
        elif k=='flushsec':
            _fdpar['flushsec'] = v
        elif k in _fdauxkey:
            pass
        elif k in _fdusagekey:
            ferr('fdset: usagekey {} allowed only for individual use in fd() itself, but not in fdset. Allowed are fd state & aux keys: {}'.format(k,_fdstateandauxkey))
        else:
            ferr('Entierly unrecognized kwarg key {} for fdset. Allowed are fd state & aux keys: {}'.format(k,_fdstateandauxkey))
def fdget(par):
    fassert.isin(par,_fdstatekey,'invalid par. Allowed are fd state keys: {}'.format(_fdstatekey))
    if par=='file':
        return _fdpar['file'].name if _fdpar['file'] else None
    else:
        return _fdpar[par]

def fd(*arg,**kwargs):
    '''
    :param arg: Arguments to be printed
    :param kwargs: Can contian (i) fdset() parameters for _fdpar, and/or (ii) any of:
        'nostdout'=True or 'nofile'=True to
            specifically suppress current outputting to stdout and/or file
        'flush'=True to currently flush indep of _fdpar['alwaysflush']
    :return: 
    '''
    fassert.arein(kwargs.keys(),_fdallkey,'kwargs not all in allowed keys')
    if kwargs:
        fdset(**{k:v for k,v in kwargs.items() if k in _fdstateandauxkey})
    s = fs(*arg)
    sameline = kwargs.get('sameline',False) or kwargs.get('sl',False)
    _end = "" if sameline else '\n'
    if _fdpar['stdout'] and not kwargs.get('nostdout',False): print(s,end=_end,flush=sameline)
    if _fdpar['file'] and not kwargs.get('nofile',False):
        sec = datetime.datetime.now().timestamp()
        flush = _fdpar['alwaysflush'] or kwargs.get('flush',False) or (_fdpar['flushsec'] and sec > _fdpar['lastsec']+_fdpar['flushsec'])
        print(s,file=_fdpar['file'],flush=flush,end=_end)
        if flush: _fdpar['lastsec'] = sec
    return s
def fdflush():
    '''Force writing accumulated fd() content in file to disk'''
    if _fdpar['file']:
        print(file=_fdpar['file'],flush=True)
        _fdpar['lastsec'] = datetime.datetime.now().timestamp()

def fdclose(disp=False):
    if _fdpar['file']:
        name = _fdpar['file'].name
        _fdpar['file'].close()
        _fdpar['file'] = None
        if disp: fd('Closed fd file {}'.name)
        return name
    else:
        if disp: fd('No fd name open; nothing to close')
        return None

def fdsl(*arg,**kwargs): # fd without newline. Cf. also https://stackoverflow.com/questions/5598181/python-multiple-prints-on-the-same-line
    kwargs['sameline'] = True
    return fd(*arg,**kwargs)
def fdcarriagereturn(*arg): # fd, re-starting at beginning of line (useful only e.g. after fdsl)
    fd('\r',*arg)
def fdslcarriagereturn(*arg): # fd without newline and re-starting at beginning of line
    print(fs('\r',*arg),end="",flush=True)
def fdc(color,*arg):
    """Write colored/formatted string. Color argument either string (cf. fdcolor.fromstr()) or fdcolor type
    Currently Windows not supported
    """
    if os.name != 'nt': # This is Windows generally
        print(fdcolor.full(color),fs(*arg),fdcolor.full(fdcolor.Reset))
    else: # In Windows, cannot currently print with fdcolor; would first have to update color code
        print(fs(*arg))
def fsx(vn): return vn+": "+fs(eval("fs("+vn+")")) # For vn a variable name string, returns name and value
def fdtmp(*arg,**kwargs): # Same as fd but to flag as developer that is only
    # Temporarily in code
    fd(*arg,**kwargs)
def fdsltmp(*arg,**kwargs): # Same as fdsl but to flag as developer that is only
    # Temporarily in code
    fdsl(*arg,**kwargs)
global _fdoncetokens
_fdoncetokens = []
def fdonce(token,*args): # For given token, write args only once
    global _fdoncetokens
    if not (token in _fdoncetokens):
        _fdoncetokens.append(token)
        fd(*args)

## String conversion
def is_number(s): # May feel cumbersome, but highly accepted on Stackoverflow, as good and not slow (!)
    try:
        float(s)
        return True
    except ValueError:
        return False

def str2bool(v):
    '''Also useful to have bool in argparse! Cf. https://stackoverflow.com/a/43357954/3673329'''
    if isinstance(v, bool):
       return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')
def argparsebool(v): return str2bool(v)

## Error message
def ferr(*args):
    # Now separately printing the message, for PyCharm compatibility, cf. https://stackoverflow.com/a/38908789/3673329
    fdc('Red','ferr: ',fs(*args))
    sys.exit(1)

def idxmax(arr): # Returns nDim indexes of max in multidimensional array arr. If multiple same max elements, first is taken
    return np.unravel_index(arr.argmax(), arr.shape)
def idxmin(arr): # Returns nDim indexes of min in multidimensional array arr. If multiple same min elements, first is taken
    return np.unravel_index(arr.argmin(), arr.shape)
def maxandidx(arr): # Returns max & indexes of max in the multidimensional array arr. If multiple same max elements, first is taken
    idx = idxmax(arr)
    return arr[idx], idx
def minandidx(arr): # Returns max & its indexes in the multidimensional array arr. If multiple same min elements, first is taken
    idx = idxmin(arr)
    return arr[idx], idx

def fround(x,nsigdig):
    """
    :brief round to nsigdig significant digits, but leave pre-floating dot numbers intact
    :param x:
    :param nsigdig:
    :return:
    """
    a = math.floor(math.log10(abs(x)))
    return round(x,max(0,nsigdig-a))

## Time/Date
def timestamp(dateonly=False):
    if dateonly:
        return datetime.date.today().strftime("%Y%m%d")
    else:
        return datetime.datetime.now().strftime("%Y%m%d_%H%M%S")

def daterange(date1, date2):
    """
    Generate dates between two dates
    :param date1: Start date
    :param date2: End date
    :return: Date between two dates. Note includes both given dates i.e. is '[]' rather than '[)'
    """
    for n in range(int((date2 - date1).days)+1):
        yield date1 + datetime.timedelta(n)

def daydelta(days): # Shortcut for the lengthy datetime.timedelta(days=days)
    return datetime.timedelta(days=days)

def weekBoundaries(year, week, doublecheck=True):  # Thanks for inspiration to https://bytes.com/topic/python/answers/499819-getting-start-end-dates-given-week-number, though I had to change bunch, also to make it correct for all years (consider iso standard rule for 4 days)
    # Note, first calendar year week acc. to ISO: is the first week with a majority (4 or more) of its days in January. Cf. https://en.wikipedia.org/wiki/ISO_week_date#First_week
    startOfYear = datetime.date(year, 1, 1)
    week0 = startOfYear - datetime.timedelta(
        days=startOfYear.isoweekday() - 1 +7*(startOfYear.isoweekday()<=4))  # week0 = last week before year's first week week 1. isoweekday has: Monday == 1 ... Sunday == 7!  Mind, by ISO std definition, first week 1 is the one which has at least 4 days in the year, i.e. for Mon until Thu i.e. for weekday <= 4, the beginning of the week of 1 Jan is already week 1, as opposed to last year's last week 'week0'
    mon = week0 + datetime.timedelta(weeks=week) # Recall, 'week0' is really the '0th' calendar week, i.e. last iso week before the year, the last week before the first calendar week
    sun = mon + datetime.timedelta(days=6)
    fassert.eq0(mon.weekday(), "Hm, does not seem to be Monday")  # Mind, weekday() has Mon=0, Sun = 6!
    fassert.eq(sun.weekday(), 6, "Hm, does not seem to be Sunday")  # Mind, weekday() has Mon=0, Sun = 6!
    fassert.eq(mon.isocalendar()[1], week, "Hm, somehow dates now are wrong calendar week")
    return mon, sun

def utctimezoneshift(utc_datetime, zone='Europe/Berlin'):
    '''
    Convert naive datetime to naive datetime shifted by utc-to-zone time difference
    Inspired by https://stackoverflow.com/a/4771733/3673329 & https://stackoverflow.com/a/100345/3673329
    Example:
        format = '%Y-%m-%d %H:%M:%S'
        utc = datetime.datetime.strptime('2011-07-21 02:37:21', format)
        loc = utctotimezone(utc,zone)
        Yields:
            datetime.datetime(2011, 7, 21, 4, 37, 21)
        i.e. added +1hr standard CET vs. UCT time diff, plus +1hr DST summertime shift
    :param utc_datetime:  naive datetime object, representing time in utc
    :param zone: str name of target zone
    :return: naive datetime with utc-to-zone time difference added
    '''

    to_zone = tz.gettz(zone)

    sec_offset = utc_datetime.astimezone(to_zone).utcoffset()
    return utc_datetime + sec_offset #datetime.timedelta(0,sec_offset)

def df_getdummies(df,colhead,prefix='',inplace=False):
    """Create dummies from category/ordinary data column in df
    :param df:
    :param colhead:
    :param prefix:
    :param inplace:
    :return: returns df and list of the created categories
    """
    # Concise solution thanks to https://stackoverflow.com/a/11589000/3673329
    elems = [str(prefix)+str(elem) for elem in df[colhead].unique()]
    df_ = df if inplace else pd.DataFrame
    for elem in elems:
        df_[elem] = df[colhead] == elem
        # df[prefix+str(elem)] = df[colhead] == elem
    return df_,elems

## Generators
def lenOfGenerator(generator):
    return sum(1 for x in generator)

## Files
def ispathonly(fo):
    '''Whether folder fo is an existing folder (not a file)'''
    fo = os.path.expanduser(fo)
    path = pathlib.Path(fo)
    return path.exists() and (not path.is_file())
def isfileonly(fn):
    '''Whether filename fn is an existing file (not a path)'''
    fn = os.path.expanduser(fn)
    path = pathlib.Path(fn)
    return path.exists() and path.is_file()
def mkdirIfNotExist(pathOrFn,disp=0, abortifemptydir=False):
    '''
    Create missing directory, including necessary subdirectories
    :param pathOrFn: if ending with '/' then interpreted as path else as filename
    :return:
    '''
    pathOrFn = os.path.expanduser(pathOrFn)
    if not os.path.exists(os.path.dirname(pathOrFn)):
        try:
            dir = os.path.dirname(pathOrFn)
            if dir:
                os.makedirs(dir)
                if disp>=1: fd('Created {}'.format(pathOrFn))
            else:
                if abortifemptydir: ferr('Dir of {} is empty (btw could also e.g. be because of backslash Win vs. slash Unix?)'.format(fn))
                elif disp>0: fd('Dir of {} is empty; not creating any directory'.format(fn))
                return
            # os.makedirs(os.path.dirname(pathOrFn))
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                fd('Error making path ',pathOrFn)
                fd('Err msg=',exc)
                raise exc
    else:
        if disp>=2: fd('Path already existed {}'.format(pathOrFn))
def filesanddirmatch(fnpattern: str):
    ''' List all files and/or paths matching fnpattern. See https://stackoverflow.com/a/3215392/3673329
    Example
        >>> fliesanddirmatch("../mypath/*.txt")
        Could return ['../mypath/file1.txt', '../mypath/dir1'] etc.
    :param fnpattern:
    :return: matching files & directories, in format exactly corresponding to dirpattern (w.r.t. relative vs. abs path, and tail '/')
    '''
    fnpattern = os.path.expanduser(fnpattern)
    return glob.glob(fnpattern)
def filesmatch(fnpattern: str):
    ''' List all files (but not paths) matching fnpattern. See https://stackoverflow.com/a/3215392/3673329
    Example
        >>> fliesmatch("../mypath/*.txt")
        Could return ['../mypath/file1.txt', '../mypath/file2.txt'] etc.
    :param fnpattern:
    :return: matching files, in format exactly corresponding to dirpattern (w.r.t. relative vs. abs path)
    '''
    fnpattern = os.path.expanduser(fnpattern)
    return [fn for fn in filesanddirmatch(fnpattern) if isfileonly(fn)]
def dirmatch(dirpattern: str):
    ''' List all directories (but not files) matching dirpattern. See https://stackoverflow.com/a/3215392/3673329
    Example
        >>> dirmatch("../mypath/*/*")
        Could return ['../mypath/dir1/dir1b', '../mypath/dir2'] etc.
    :param dirpattern:
    :return: matching directories, in format exactly corresponding to dirpattern (w.r.t. relative vs. abs path, and tail '/')
    '''
    dirpattern = os.path.expanduser(dirpattern)
    return [fn for fn in filesanddirmatch(dirpattern) if ispathonly(fn)]

def filesinpath(path,abortifnotpath=True):
    """
    List all files (not sub-paths) in path
    Cf. also https://stackoverflow.com/a/3207973/3673329
    Note, to instead recognize patterns, could also use import glob; print(glob.glob("/home/adam/*.txt")), cf. https://stackoverflow.com/a/3215392/3673329
    :param path:
    :param abortifnotpath:
    :return:
    """
    path = os.path.expanduser(path)
    if not os.path.exists(os.path.dirname(path)):
        if abortifnotpath:
            ferr("Path ",path," does not exist")
        return []
    _, _, filenames = next(os.walk(path), (None, None, []))
    return filenames
def dirsinpath(path,abortifnotpath=True):
    """ Sub-Directories (aka sub-paths) in path
    List all directories (not files) in path
    Cf. also https://stackoverflow.com/a/3207973/3673329
    Note, to instead recognize patterns, could also use import glob; print(glob.glob("/home/adam/*.txt")), cf. https://stackoverflow.com/a/3215392/3673329
    :param path:
    :param abortifnotpath:
    :return:
    """
    path = os.path.expanduser(path)
    # NOTE NOT TESTED YET
    if not os.path.exists(os.path.dirname(path)):
        if abortifnotpath:
            ferr("Path ",path," does not exist")
        return []
    (_, dirnames, _) = next(os.walk(path))
    # _, _, filenames = next(os.walk(path), (None, None, []))
    # _, dirnames, _ = next(os.walk(path), (None, None, []))
    return dirnames
def freespaceinfolderMB(path): # Free space in filesystem containing folder,
    #  in Megabytes
    path = os.path.expanduser(path)
    statvfs = os.statvfs(path)
    freeuseablebites = statvfs.f_frsize * statvfs.f_bavail
    return freeuseablebites/1048576 # 1 MB = 1024 * 1024 bytes
def get_sizeMB(start_path = '.'): # Size of all files in folder incl.
    # subfolders, essentially from https://stackoverflow.com/a/1392549/3673329
    start_path = os.path.expanduser(start_path)
    total_size = 0
    for dirpath, dirnames, filenames in os.walk(start_path):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            total_size += os.path.getsize(fp)
    return total_size/1048576 # 1 MB = 1024 * 1024 bytes
def ensureextension(fnb,ext):
    """ Adds .ext to filename fnb, unless fnb already ends with .ext. When ext already has the leading ., it will not be duplicated
    :param fnb: string filename or filenamebase, e.g. 'D:\temp\myfile.txt' or 'D:\temp\myfile'
    :param ext: filename extension, e.g. '.csv' or '.txt', with or w/o the '.' (will be added if missing)
    :return:
    """
    if ext[0]!='.': ext = '.'+ext
    fn = fnb + (ext if not fnb.endswith(ext) else '')
    return fn
def fn_concat(*arg):
    s = ''
    sep = '\\' if os.name == 'nt' else '/' # Windows vs. Linux. Note, 'nt' is Windows generally
    for a in arg:
        if s and a:
            count = 1*(s[-1]==sep) + 1*(a[0]==sep)
            # count = 1*(s[-1]=='\\') + 1*(a[0]=='\\')
            if count==1:
                s+=a
            elif count==0:
                s+=sep+a
                # s+='\\'+a
            else:
                s+=a[1:]
        else:
            if not s: s=a
    return s

## CSV
def dicttocsv(mydict, fnb):
    fnb = os.path.expanduser(fnb)
    fn = ensureextension(fnb,'csv')
    with open(fn, 'w') as csv_file:
        writer = csv.writer(csv_file)
        for key, value in mydict.items():
           writer.writerow([key, value])

## Time-series
def MA(ts, radius=None, width=None,mode="NAN"): # mode {NAN,const,trim}: Whether to fill undefined ends (of length=radius=width/2) with NAN or const, or whether trim these away (returning series with the undefined ends removed)
    fassert.f((radius==None) != (width==None),fs("Exactly one of the two, radius and width, must be provided as not-None, but here radius="+str(radius)," and width="+str(width)))
    n = len(ts)
    fassert.f(["NAN","const","trim"])
    if width!=None:
        fassert.eq(width%2,1,"Width must be impair integer (or None, if radius provied instead)")
        radius=int((width-1)/2)
    else:
        width=2*radius+1
    fassert.ge(n,width,"ts shorter than kernel")
    ma = np.zeros(n-2*radius)
    ma[0]=sum(ts[:width])
    for i in range(1,n-2*radius):
        if np.isnan(ma[i-1]):
            ma[i]=sum(ts[i:(width+i)]) # Computationally inefficient way to avoid perpetuating possible NAN
        else:
            ma[i]=ma[i-1]-ts[i-1]+ts[i+width-1]
    if mode!="trim":
        if mode == "NAN":
            val = np.zeros(radius)
            val.fill(np.nan)
            ma=np.concatenate((val,ma,val))
        else:
            val0=valE = np.zeros(radius)
            val0.fill(ma[0])
            valE.fill(ma[-1])
            ma=np.concatenate((val0,ma,valE))
        fassert.eq(len(ts),len(ma),"len(ts)!=len(ma)")
    ma/=width
    fassert.ge(np.nanmax(ts),np.nanmax(ma),"Hm, ma max logically had to be at least ts max")
    fassert.ge(np.nanmin(ma),np.nanmin(ts),"Hm, ma min logically had to be at max ts min")
    return ma

def mypcolor(data, ax=None, basetitle="", xlab=None, ylab=None, xval=None, yval=None, bestdir="max", colorbar=True, dobest=True):
    if not ax: fig, ax = plt.figure()
    pc = ax.pcolor(data.T)
    if colorbar: plt.colorbar(pc)
    nx,ny=data.shape
    if xlab: ax.set_xlabel(xlab)
    if ylab: ax.set_ylabel(ylab)
    if xval is None: xval = range(nx)
    else: fassert.eq(nx,len(xval))
    if yval is None: yval = range(ny)
    else: fassert.eq(ny,len(yval))
    ax.set_xticks(np.arange(nx)+.5)
    ax.set_xticklabels(xval)
    ax.set_yticks(np.arange(ny)+.5)
    ax.set_yticklabels(yval)
    if dobest:
        bestval, idxbest = maxandidx(data) if bestdir is "max" else minandidx(data)
        ibestx, ibesty = idxbest
        # bestval, ibestx, ibesty = bestinarray(data,bestdir)
        bestx = xval[ibestx]
        besty = yval[ibesty]
        ax.scatter(ibestx+.5,ibesty+.5)
        besttitle=fs(". ", bestdir,"=", np.round(bestval,3),"@", bestx, "/", besty)
    else:
        besttitle=""
    ax.set_title(fs(basetitle,besttitle))
    if dobest:
        return bestval, bestx,besty, ibestx, ibesty
    else:
        return None

def show():
    """
    Show the graphs w/o interrupting program. So like pyplot's show() but w/o interrupting
        Convenient also as lets user debug program on console w/o first closing all graphs
    :return:
    """
    plt.draw()
    plt.gcf().canvas.flush_events()
    plt.pause(1e-17)  # Crucial, otherwise the picture updating doesn't happen!

from matplotlib.backends.backend_pdf import PdfPages

def saveplots(fnb, figs=None, dpi=200,addtimestamp=True,disp=True):
    """Print ot PDF (i) all open plots (figs=None) or (ii) all plots from iterable figs
    Thanks to some stackoverflow (or so) post, though I lost the link
    """
    fn = f'{fnb}{flib.timestamp() if addtimestamp else ""}.pdf'
    pp = PdfPages(fn)
    if figs is None:
        figs = [plt.figure(n) for n in plt.get_fignums()]
    for fig in figs:
        fig.savefig(pp, format='pdf')
    if disp: fd(f'Saved {len(figs)} figures in {fn}')
    pp.close()



# SectionGuy: For 'with' statement with automatic closing
#   Based on https://www.geeksforgeeks.org/with-statement-in-python/
#   And for Exception handling on https://stackoverflow.com/a/22417454/3673329
class SectionGuy(object):
    def __init__(self, sectionname, disp=True):
        self.sectionname = sectionname
        self.disp = disp

    def __enter__(self):
        if self.disp: fd(self.sectionname,":")
        return self.sectionname

    def __exit__(self, exc_type, exc_value, tb):
        if exc_type is not None:
            fd(self.sectionname,' Error:')
            traceback.print_exception(exc_type, exc_value, tb)
            # return False # uncomment to pass exception through
        else:
            if self.disp: fd(self.sectionname, ' done.')
        return True

class FrozenDict(collections.Mapping):
    # From https://stackoverflow.com/a/2704866/3673329
    """Don't forget the docstrings!!"""

    def __init__(self, *args, **kwargs):
        self._d = dict(*args, **kwargs)
        self._hash = None

    def __iter__(self):
        return iter(self._d)

    def __len__(self):
        return len(self._d)

    def __getitem__(self, key):
        return self._d[key]

    def __hash__(self):
        # It would have been simpler and maybe more obvious to
        # use hash(tuple(sorted(self._d.iteritems()))) from this discussion
        # so far, but this solution is O(n). I don't know what kind of
        # n we are going to run into, but sometimes it's hard to resist the
        # urge to optimize when it will gain improved algorithmic performance.
        if self._hash is None:
            hash_ = 0
            for pair in self.iteritems():
                hash_ ^= hash(pair)
            self._hash = hash_
        return self._hash


# Desperate trivial interaction: using file: Halter file
# Write '1' into halter file (std = tmp/myhalter.txt ), to have readhalter() return True so programs can stop
def inithalter(fnb='tmp/myhalter',addunique=True, disp=True):
    '''
    See also: readhalter
    :param fnb:
    :param addunique:
    :param disp:
    :return:
    '''
    fn = fnb + \
         (''.join(random.choices(string.ascii_letters + string.digits, k=10)) if addunique else '') + \
        '.txt'
    mkdirIfNotExist(fn)
    readhalter.fn=fn
    with open(fn,'w') as f:
        print(0, file=f, flush=True)
    if True: fd('Initialized halter file ',fn)
    readhalter.time = time.time()

def readhalter(sec=3,disp=1,autoinit=False):
    '''Return if file content is "1" (instead of e.g. initialized 0). Mind, do not add any newline!
    See also: inithalter() [needs to be run before readhalter]
    '''
    now = time.time()
    try:
        dummy = readhalter.time # Note, readhalter.time is created by inithalter()
    except Exception as e:
        if autoinit:
            try:
                inithalter()
            except:
                ferr('Hm, readhalter: autoinit with inithalter failed')
        else:
            ferr('readhalter error A: You forgot to initialize with inithalter()? use autoinit=True to initialize automatically!')
    try:
        timesincelast = now - readhalter.time # Note, readhalter.time is set by inithalter()
    except Exception as e:
        ferr('readhalter error B: You forgot to initialize with inithalter()? Use autoinit=True to initialize automatically!')
    readhalter.time = now
    if timesincelast > sec:
        try:
            with open(readhalter.fn, 'r') as f:
                s = f.readline(1000)
            halt = s=='1'
            if halt and disp>0: fd('Halting because s<>1 in {}. s={}'.format(readhalter.fn,s))
            return halt
        except:
            fdonce('-- Unable to verify halter file ',readhalter.fn)
            return False
    else:
        return False

def whereisthisrun():
    # Thanks for basis to https://stackoverflow.com/a/35139786/3673329
    return psutil.Process(os.getpid()).parent().name()