# Lil' AI (tensorflow, rllib, ..) helpers

import numpy as np
from typing import Union, List, Dict, Callable, Any, Tuple
from flib.flib import fs as fs
from flib.flib import fsfw as fsfw
from flib.flib import fd as fd
from flib.flib import fdsl as fdsl
from flib.flib import fdc as fdc
from flib.flib import ferr as ferr


def actionmap(actraw, rng: List):
    """ Maps unbounded action actraw into range [rng[0],rng[1]] (or maybe w/o bounds, (rng[0],rng[1]))
    :param actraw: Action that can be scalar, list, dictionary, etc., and nestings of it
    :param rng: List (or equivalent) with Lower bound, rng[0], and upper bound, rng[1] for target range
    :return: action object of structure identical to actraw, but with all values mappped from (-np.inf,np.inf) to (rng[0],rng[1])
    Note, may be useful to use manual action mapping from infinite to finite bounds for RLlib PPO; in my experiments that is the case,
    and bbalaji discusses it here: https://github.com/ray-project/ray/issues/1862
    """
    if np.isscalar(actraw):
        # Sigmoid(x) = 1 / (1 + math.exp(-x)), e.g. https://stackoverflow.com/questions/3985619/how-to-calculate-a-logistic-sigmoid-function-in-python
        return rng[0] + 1 / (1 + math.exp(-actraw)) * (rng[1] - rng[0])
    elif isinstance(actraw, dict):
        return {k: actionmap(val,rng) for k, val in actraw.items()}
    elif isinstance(actraw, list):
        return [actionmap(v,rng) for v in actraw]
    elif isinstance(actraw, np.ndarray):
        return rng[0] + 1 / (1 + np.exp(-actraw)) * (rng[1] - rng[0])
    else:
        ferr('Unexpected type of actraw, %s' % type(actraw))
        return None


def setLR(config:dict,algo:str,lr:float,disp=False):
    '''Set learning-rate for RLlib agent configs'''
    if lr is not None:
        if disp: fd('Setting lr {:.1e}'.format(lr))
        if algo in ['PPO','PG','A2C','APPO']:
            config['lr'] = lr
        elif algo in ['TD3']:
            config['actor_lr'] = lr
            config['critic_lr'] = lr
            config['lr'] = lr # Probably not required, but as TD3 std config also contains this, I add it too to be sure
        elif algo in ['DDPG']:
            config['actor_lr'] = lr
            config['critic_lr'] = lr
        else:
            ferr('flib ai: rllibSetLR: lr setting not yet implemented for {}'.format(algo))


def getweightsetc(agent):
    '''Store (return) agent status as follows:
        weights
        iteration num
        [are other variables that should be stored?]
    Note, genuine environment 'state' is not stored
    See also: setweightsetc
    '''
    ferr('Update: Mind, for now instead rllib"s save_to_object/restore_from_object is better')
    return {'weights': agent.get_weights(), 'it': agent.iteration}

def setweightsetc(agent,weightsetc,reset=False):
    '''Reset agent status to weightsetc:
        weights
        iteration num
        [are other variables that should be reloaded?]
    Note, genuine environment 'state' is not restored; you may want to run a reset
    See also getweightsetc
    :param agent:
    :param weightsetc:
    :param reset:
    :return:
    '''
    ferr('Update: Mind, for now instead rllib"s save_to_object/restore_from_object is better')

    agent.set_weights(weightsetc['weights'])
    agent.iteration = weightsetc['it']
